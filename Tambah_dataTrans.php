<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Data Transaksi</h2>
  <form action="proses_inputdataTrans.php" method="POST">
    <div class="form-group">
      <label for="id_transaksi">Id Transaksi:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Id Transaksi" name="id_transaksi">
    </div>
    <div class="form-group">
      <label for="pwd">Nama Customer:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Nama Customer" name="nama_customer">
    </div>
    <div class="form-group">
      <label for="pwd">Merk Kamera:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Merk Kamera" name="merk_kamera">
    </div>
    <div class="form-group">
      <label for="pwd">Jenis Kamera:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Jenis Kamera" name="jenis_kamera">
    </div>
    <div class="form-group">
      <label for="pwd">Keluhan:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Keluhan" name="keluhan">
    </div>
     <div class="form-group">
      <label for="pwd">No Telepon:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan No Telepon" name="no_tlp">
    </div>
     <div class="form-group">
      <label for="pwd">Tanggal:</label>
      <input type="date" class="form-control" id="pwd" placeholder="Masukkan Tanggal" name="tanggal">
    </div>
    
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember me
      </label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

</body>
</html>
