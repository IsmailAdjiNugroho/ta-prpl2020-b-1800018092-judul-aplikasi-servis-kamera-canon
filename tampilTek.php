<?php
include 'koneksi.php';
$data = mysqli_query($koneksi, "SELECT *FROM teknisi");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Servis Kamera</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Servis Kamera</a>
    </div>
    <ul class="nav navbar-nav">
      
      <li><a href="tampilHom.php">Home</a></li>
      <li><a href="tampilKam.php">Kamera</a></li>
      <li><a href="tampilTek.php">Teknisi</a></li>
      <li><a href="tampilSer.php">Servis</a></li>
      <li><a href="tampilTransaksi.php">Transaksi</a></li>
     
    </ul>
    
      
    
  </div>
</nav>

<div class="container">
</div>



<table border="10">
  <tr>
    <td>Kode Teknisi</td>
    <td>Nama</td>
    <td>Alamat</td>
    <td>No telepon</td>
    <td>Action</td>
  </tr>
  <?php foreach ($data as $value): ?>
  <tr>
    <td><?php echo $value['kd_teknisi'] ?></td>
    <td><?php echo $value['nama_teknisi'] ?></td>
    <td><?php echo $value['alamat_teknisi'] ?></td>
    <td><?php echo $value['tpl_teknisi'] ?></td>
    <td>
      <div class="btn-grup-horizontal">
        <button type="button" class="btn btn-default"><a href="hapusTeknisi.php?kd_teknisi=<?php echo $value['kd_teknisi']?>">Hapus</a></button>
      </div></td>
    </tr>
  </tr>
  <?php endforeach ?>
</table>
  <!-- <button type="button" class="btn btn-success">Success</button> -->
<!-- <a href="Tambah_data.php">Tambah data</a> -->
<button type="button" class="btn btn-default"><a href="Tambah_dataTeknisi.php">Tambah Data</a></button>
</body>
</html>