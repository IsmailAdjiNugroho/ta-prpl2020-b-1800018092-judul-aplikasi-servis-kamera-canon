<?php
include 'koneksi.php';
$data = mysqli_query($koneksi, "SELECT *FROM servis");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Servis Kamera</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Servis Kamera</a>
    </div>
    <ul class="nav navbar-nav">
      
      <li><a href="tampilHom.php">Home</a></li>
      <li><a href="tampilKam.php">Kamera</a></li>
      <li><a href="tampilTek.php">Teknisi</a></li>
      <li><a href="tampilSer.php">Servis</a></li>
      <li><a href="tampilTransaksi.php">Transaksi</a></li>
     
    </ul>
    
      
   
  </div>
</nav>

<div class="container">
</div>



<table border="10">
  <tr>
    <td>No Servis</td>
    <td>Tanggal</td>
    <td>Kode Teknisi</td>
    <td>Jenis Kamera</td>
    <td>Total</td>
    <td>Keluhan</td>
    <td>Action</td>
  </tr>
  <?php foreach ($data as $value): ?>
  <tr>
    <td><?php echo $value['no_servis'] ?></td>
    <td><?php echo $value['tanggal'] ?></td>
    <td><?php echo $value['kd_teknisi'] ?></td>
    <td><?php echo $value['jenis_kamera'] ?></td>
    <td><?php echo $value['total'] ?></td>
    <td><?php echo $value['keluhan'] ?></td>
    <td>
      <div class="btn-grup-horizontal">
        <button type="button" class="btn btn-default"><a href="hapusServis.php?no_servis=<?php echo $value['no_servis']?>">Hapus</a></button>
      </div></td>
  </tr>
  <?php endforeach ?>
</table>
  <!-- <button type="button" class="btn btn-success">Success</button> -->
<!-- <a href="Tambah_data.php">Tambah data</a> -->
<button type="button" class="btn btn-default"><a href="Tambah_dataServis.php">Tambah Data</a></button>
</body>
</html>