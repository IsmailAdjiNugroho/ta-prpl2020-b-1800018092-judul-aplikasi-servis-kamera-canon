<?php
include 'koneksi.php';
$data = mysqli_query($koneksi, "SELECT *FROM servis_kamera");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Servis Kamera</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Servis Kamera</a>
    </div>
    <ul class="nav navbar-nav">
      
      <li><a href="tampilHom.php">Home</a></li>
      <li><a href="tampilKam.php">Kamera</a></li>
      <li><a href="tampilTek.php">Teknisi</a></li>
      <li><a href="tampilSer.php">Servis</a></li>
      <li><a href="tampilTransaksi.php">Transaksi</a></li>
    
    </ul>
    <form class="navbar-form navbar-left" action="/action_page.php">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Cari" name="Cari">
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
  </div>
</nav>

<div class="container">
</div>

<center>
<div class="uk-container" style="padding-top:30px">

    <div class="uk-tile uk-tile-primary">
            <p class="uk-h1" style="font-family:rockwell;">Selamat Datang di<br>Service Kamera Canon</p>
    </div>

    <div class="uk-tile uk-tile-muted">
            <p class="uk-h5" style="font-family:rockwell;">SERVICE KAMERA CANON merupakan Aplikasi Service Kamera berbasis WEB.<br></p>
    
            <p align=left class="uk-h5" style="font-family:rockwell; padding-left:80px;">Cara melakukan reservasi service cukup mudah :
                <br>&emsp;1. Daftar dan Login Akun
                <br>&emsp;2. Isi Data Kamera
                <br>&emsp;3. Pilih Jadwal yang Tersedia
                <br>&emsp;4. Reservasi Berhasil
                <br>&emsp;5. Datanglah ke Toko dengan membawa Kamera yang ingin diperbaiki
            </p>
    </div>
</div>
</center>

</body>
</html>